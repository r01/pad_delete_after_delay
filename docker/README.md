# Dockerfile for `delete_pad_after_delay`

This directory only contains an init script. It will grab the `$DIR`, `$URL` and `$DEL` environment variables and pass them to `delete_pad_after_delay`. The API key will be read from a file specified with the `$APIKEY_PATH` variable (it must be mounted or written somehow). You could check picasoft's docker-compose.yml as an example. Only one instance is supported, it is (internally) named `instanceD`.
